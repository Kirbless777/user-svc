import { Handler, Context } from 'aws-lambda';
import dotenv from 'dotenv';
import path from 'path';
// const dotenvPath = path.join(__dirname, '../', `config/.env.${process.env.NODE_ENV}`);
const dotenvPath = path.join(__dirname, '../', 'config/.env');
dotenv.config({
  path: dotenvPath,
});

import { user } from './model';
import { UsersController } from './controller/users';
const userController = new UsersController(user);

export const userCreate: Handler = (event: any, context: Context) => userController.create(event, context);
export const userSignIn: Handler = (event: any, context: Context) => userController.signIn(event, context);
export const userMe: Handler = (event: any, context: Context) => userController.me(event, context);
export const userVerify: Handler = (event: any, context: Context) => userController.verify(event, context);
export const userAuth: Handler = (event: any, context: Context) => userController.auth(event, context);
export const getUsers: Handler = (event: any, context: Context) => userController.getUsers(event, context);
