import { FilterQuery, Model } from 'mongoose';
import { MongoUser } from '../model/interfaces/user.interface';
import { CreateUserDTO, UserResponse, SignInCredentials } from '../model/dto/createUserDTO';
import MG from 'mailgun-js';
import * as jwt from 'jsonwebtoken';
import { internalError, throwApiError, throwGeneratedApiError } from '../utils/errors';

export class UsersService {
  private users: Model<MongoUser>;

  // TODO - update to global variables and removed hardcoded keys!!!
  readonly env = {
    VALIDATE_PRIVATE_KEY: 'LS0tLS1CRUdJTiBFQyBQUklWQVRFIEtFWS0tLS0tCk1IY0NBUUVFSUtsbGdLSzd6eVc3MkIrK3M1UFlCT3c2d2s4YVNkT0h0N0Nyc0YreVBnRHRvQW9HQ0NxR1NNNDkKQXdFSG9VUURRZ0FFS0R3bGcrT3BqYU45S3JlMTN3aTUwOVYvMHh2MnUyWEFKSTRXOUo2OE45bFhRdHhIRW9sZAo0WmoxdW01djFZMHdTTlcwaVhZdzdva1hXejJDVmtQNW9RPT0KLS0tLS1FTkQgRUMgUFJJVkFURSBLRVktLS0tLQ==',
    PRIVATE_KEY: 'LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2d0lCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktrd2dnU2xBZ0VBQW9JQkFRQ1pENmJQeC81ZDlJVHUKL09PT3Y3ZFBpV2hSWFhZZDNRRVAvUys3RUtpNkxLVE9FcEJsdEVDakJ4d2JGUk1qcHBwTEt0R2xVUnNxM1pERQovdDVzeVIxNU13cGhpeEpyNEl6dUY2Q3lsK0o4S1RkdWhMRnd0RWVJY1EzVjBoeGZtWXdtVGROQ2lxNCtGRC9uCnE0bC9Ka0l0eUFXcWpmN0hUcjhYRXNLNEdheFIzRWR5OHdqdm5lY1VtNlRUR1RFRUR0VWU2MEcrQVRpb2FMbGcKUERnZGJ3QlhndWxTbVJBU0NIL2pieEFZeUNmRkdiM1k3bXNheEdPVmlxZVV5WXJyeEIwQ2VYcFBCcDVUM0hrLwpvT2UwanNRWXA3SUtJZjJOWXhzSXpuYWZ2emZ3YnRhbC95b2t1Yk84aHFRS254K2NEKzJNK244NDVlOUE4aWJkCjF2dVg2M3BWQWdNQkFBRUNnZ0VBUjV4aENUbWlzc2FpSTBaQ0dSbHRaRkwvY1lIdXIwNGxIeHZ2NXUrR1lZUmQKbnVSa2xwRGM1T21BcHRNWFgwaWxTNkcrOEQrZ0hGQUpXWUY5QXh5ZEk0VlR5QXRQRlJXOEdmRUlDRWF6ZWkrMgpkWWl6aysxa3dsdFNnOFdIN05wa0RjKy9sWlJZclViQUYxM1VPSjdudTQrbnh3SWtQNCtDTEFObEFzbFFMQVBRClVVM0RidnBFVGE1UW41Y0xLTFdHVWFDQ0hUaE9kS081YUh3Wk5ZSHMwdG03M2NjWDVDSVFLbGlzWkxvQmNSSXoKb3lNQ25zVWpoMWhkUTdIeGR3SDIreGYwRnBnSGNGQklEbDgvVWw3Tmo2djZuK21iZiszUXBLWXozbFRDQ2JVSgpKSzN5WG9tQTJLWGx5VkFuZ284WlF1Q2owTmQzOWlCWXJJdUdTMXphQ1FLQmdRREpSOXNGL0dzNUhWY2l2QTZvCkhYbXpxK1IxVGVlOS84NjVnNUpnR3RjYUNjNWsvVDNzbzZHR1ZzVzJJeDdBaWxqT084ZHQyeUVnOGxlTUp5TWkKTE5KenE2Z3FrS3diRnNwWUdKY0pPZXJHSllkT3dXN2syRkRhRHVBMG9uVE53cGtCVFBaWE5UOU1Rb3lpeDQ1agpnN0pEaEZPOVVXMGg2d1R1dVFBSlpJYmwzd0tCZ1FEQ3EvUDh3L1lXZXZZS2NzQWF5MzRMa0tpd2tpVEZFeFdkCnBEWUN6NVp0YXprUHFzOHhrSHZQeUtyM0J4L3M5Tk1aQTZQeG9PUFlydlRBUVhUZlpUSXFyS3dEdXBRcjFselIKUTB3MnFsaW1ld2wxYjVJd2RMMG5zUHhsbGVQbXh1NmJQWTZORVBGQUdkSjZCNmRZZDhId04yeUNJMXVLUjNKZApNSk9wZlVvZVN3S0JnUUN5dWJRNDUzOEtFd3g1VWtVdGNDYk5ySGRLMWVkTjVhUlpSQVFvaEF5MkdUa0VvWDFuCjc1QVFxbUlhcmFxenl1UzZFNGlzK21PN1RwVUFaUXdrWDJwcnZXT3drQS9TYWZVNExuV0dnci8rTy9xZXF0SGUKTUZueWxqenRYOGVab1E2ZGdncEhPNGUrSG1Xa2NNLzFlMDVCOTNFbjdQS2JxSVVUdUgvWTEwT1ZQd0tCZ1FDdwpTYTlGY3Qvak02NmpmNDdmdE8rTDRaQWhkZmErVUszK2Q3cEJpV01iNjBGZElyMUdrU1RONFZhUkpXNlAwcFZuCnpaZ2xOQ0doRTY2b2U1NHV2eUZTNjlOSi9TZzVHb0lyTHRwUmxxbDQrV3B6WW5uc1Q0c0w2b1ptSi9xZC9nQU4KclF6dVNpMXVjdlBiZGhyaFRuSGQ2eExrTnNpcXpGQ1FlM0Q4eDJ6YlNRS0JnUURHWDZvU1cxVms3VWtDUDNDUgpvYzZxZmxYSW04citxTHJLdWZNWXp3RituQnVveEYrNWRUMUJWc1ZXSVd2SllXSmNqcFE4NURoMXlzakJEWksrCmdyaSsxZFY4QVZtQkk0SDBiNDJYb2txRFFCNWlHQTJsYWE5bVFrYzVqaXc3ZHozNy9WdmVqWWdxdDFjZzNsMFYKR3dJR1I1RDZjaHhSZUFsdlVqTkpNMUxjRlE9PQotLS0tLUVORCBQUklWQVRFIEtFWS0tLS0t',
    PUBLIC_KEY: 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFtUSttejhmK1hmU0U3dnpqanIrMwpUNGxvVVYxMkhkMEJELzB2dXhDb3VpeWt6aEtRWmJSQW93Y2NHeFVUSTZhYVN5clJwVkViS3QyUXhQN2ViTWtkCmVUTUtZWXNTYStDTTdoZWdzcGZpZkNrM2JvU3hjTFJIaUhFTjFkSWNYNW1NSmszVFFvcXVQaFEvNTZ1SmZ5WkMKTGNnRnFvMyt4MDYvRnhMQ3VCbXNVZHhIY3ZNSTc1M25GSnVrMHhreEJBN1ZIdXRCdmdFNHFHaTVZRHc0SFc4QQpWNExwVXBrUUVnaC80MjhRR01nbnhSbTkyTzVyR3NSamxZcW5sTW1LNjhRZEFubDZUd2FlVTl4NVA2RG50STdFCkdLZXlDaUg5aldNYkNNNTJuNzgzOEc3V3BmOHFKTG16dklha0NwOGZuQS90alBwL09PWHZRUEltM2RiN2wrdDYKVlFJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t',
    VALIDATE_PUBLIC_KEY: 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFS0R3bGcrT3BqYU45S3JlMTN3aTUwOVYvMHh2Mgp1MlhBSkk0VzlKNjhOOWxYUXR4SEVvbGQ0WmoxdW01djFZMHdTTlcwaVhZdzdva1hXejJDVmtQNW9RPT0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t',
  };

  constructor(users: Model<MongoUser>) {
    this.users = users;
  }

  protected async createUser(user: CreateUserDTO): Promise<UserResponse> {
    this.checkValidUser(user);
    if (!user.profileImage || user.profileImage.length === 0) {
      user.profileImage = 'https://ryan-sermons.s3.eu-west-2.amazonaws.com/media/user.png';
    }
    try {
      const dbUser = await this.users.create(this.formatDataToUser(user));
      const validateToken = await this.createJwtValidateToken(user.email);
      await this.validateEmail(dbUser, validateToken);
      return this.formatDbUserToUserResponse(dbUser);
    } catch (err) {
      console.log(err);
      if (err.code) {
        throw throwApiError(err.code);
      }
      throw err;
    }
  }

  protected async userSignIn(cred: SignInCredentials): Promise<UserResponse> {
    return new Promise((res, rej) => this.users
      .findOne(
        { email: cred.email },
        (err, dbUser) => {
          if (err) rej(err);
          if (!dbUser) {
            rej(throwApiError('email-password'));
          } else {
            dbUser.comparePassword(cred.password, async (err, isMatch) => {
              if (!dbUser.registered) rej(throwApiError('user-not-validated'));
              if (err) rej(err);
              if (!isMatch) {
                rej(throwApiError('email-password'));
              } else {
                try {
                  const token = await this.createJwtToken(cred.email);
                  res(this.formatDbUserToUserResponse(dbUser, token));
                } catch (e) {
                  rej(internalError(e));
                }
              }
            });
          }
        }),
    );
  }

  private checkValidUser(user: CreateUserDTO): void {
    if (!user) {
      throw throwApiError('no-user-body');
    }
    const requiredFields: string[] = ['firstName', 'surname', 'password', 'email'];
    const missingFields: string[] = [];
    requiredFields.forEach((f: string) => {
      if (!user[f]) {
        missingFields.push(f);
      }
    });
    if (missingFields.length > 0) {
      throw throwGeneratedApiError('no-user-validation', missingFields);
    }
  }

  protected updateUser(id: number, data: object) {
    return this.users.findOneAndUpdate(
      { id },
      { $set: data },
      { new: true },
    );
  }

  protected findUsers() {
    return this.users.find();
  }

  protected findOneUserById(id: string) {
    return this.users.findOne({ id });
  }

  protected deleteOneUserById(id: string) {
    return this.users.deleteOne({ id });
  }

  protected findUsersByEmail(email: string[], includeId: number) {
    return this.users.find(
      { email } as FilterQuery<string[]>,
      { _id: includeId, firstName: 1, surname: 1, email: 1, profileImage: 1 }
    );
  }

  protected async findUserByToken(token: string): Promise<UserResponse> {
    const email = await this.extractEmailFromToken(token);
    const dbUser: MongoUser = await this.users.findOne({ email });
    if (dbUser && dbUser._id) {
      return this.formatDbUserToUserResponse(dbUser);
    }
    throw throwApiError('user-not-found');

  }

  protected async verifyUser(token: string): Promise<UserResponse> {
    const email = await this.verifyValidUser(token);
    const dbUser: MongoUser = await this.users.findOneAndUpdate({ email }, { registered: true });
    if (dbUser && dbUser._id) {
      const token = await this.createJwtToken(email);
      return this.formatDbUserToUserResponse(dbUser, token);
    }
    throw throwApiError('user-not-found');

  }

  protected async authUser(token: string): Promise<boolean> {
    return await this.authenticateUser(token);
  }

  private async verifyValidUser(token: string): Promise<string> {
    const buff = new Buffer(this.env.VALIDATE_PUBLIC_KEY, 'base64');
    const publicKey = buff.toString('ascii');

    // TODO make more robust
    try {
      const decoded = await jwt.verify(token, publicKey, { algorithms: ['ES256'] });
      return decoded['sub'];
    } catch (e) {
      console.log(e);
      throw throwApiError('invalid-token');
    }
  }

  private async authenticateUser(token: string): Promise<boolean> {
    const buff = new Buffer(this.env.PUBLIC_KEY, 'base64');
    const publicKey = buff.toString('ascii');

    // TODO make more robust
    try {
      const decoded = await jwt.verify(token, publicKey);
      return !!decoded;
    } catch (e) {
      console.log(e);
      throw throwApiError('invalid-token');
    }
  }

  private formatDataToUser(user: CreateUserDTO): CreateUserDTO {
    return {
      firstName: user.firstName,
      surname: user.surname,
      email: user.email,
      profileImage: user.profileImage,
      knownAs: user.knownAs,
      password: user.password,
    };
  }

  private formatDbUserToUserResponse(user: MongoUser, token?: string): UserResponse {
    const formattedUser: UserResponse = {
      id: user._id,
      firstName: user.firstName,
      surname: user.surname,
      email: user.email,
      profileImage: user.profileImage,
      knownAs: user.knownAs ? user.knownAs : null,
    };
    if (token) {
      formattedUser.token = token;
    }
    return formattedUser;
  }

  private async createJwtValidateToken(email: string): Promise<string> {
    const buff = new Buffer(this.env.VALIDATE_PRIVATE_KEY, 'base64');
    const privateKey = buff.toString('ascii');

    const data = {
      iss: 'svc-auth',
      aud: 'svc-auth',
      sub: email,
      iat: Math.floor(Date.now() / 1000),
      nbf: Math.floor(Date.now() / 1000),
      exp: Math.floor(Date.now() / 1000) + (60 * 60),
    };
    try {
      return await jwt.sign(data, privateKey, { algorithm: 'ES256' });
    } catch (e) {
      console.log(e);
      return '';
    }
  }

  private async createJwtToken(email: string): Promise<string> {
    const buff = new Buffer(this.env.PRIVATE_KEY, 'base64');
    const privateKey = buff.toString('ascii');

    const data = {
      iss: 'svc-auth',
      aud: 'svc-auth',
      sub: email,
      iat: Math.floor(Date.now() / 1000),
      nbf: Math.floor(Date.now() / 1000),
      exp: Math.floor(Date.now() / 1000) + (60 * 60),
    };
    return jwt.sign(data, privateKey, { algorithm: 'RS256' });
  }

  private async validateEmail(user: MongoUser, token: string): Promise<boolean> {
    const mailgun = MG({
      apiKey: '3dc069ea1a2a514bc2401f27c18b6246-3d0809fb-60728a74',
      domain: 'battleslaps.com',
      host: 'api.eu.mailgun.net',
    });

    try {
      const data = {
        from: 'BattleSlaps <info@battleslaps.com>',
        to: user.email,
        subject: 'Verify Account',
        html: this.emailTemplate(user, token),
      };

      const send = await mailgun.messages().send(data);
      console.log(send);
      return !!send;
    } catch (e) {
      console.log(e);
      // todo: throw correct error
      throwApiError('user-not-validated');
    }
  }

  private async extractEmailFromToken(token: string): Promise<string> {
    const buff = new Buffer(this.env.PUBLIC_KEY, 'base64');
    const publicKey = buff.toString('ascii');

    // TODO make more robust
    try {
      const decoded = await jwt.verify(token, publicKey);
      return decoded['sub'];
    } catch (e) {
      console.log(e);
      throw throwApiError('invalid-token');
    }
  }

  readonly emailTemplate = (user: MongoUser, token: string): string => `
<div>
    <h1>Welcome to BattleSlaps, ${user.firstName}</h1>
    <h2>Please verify your account</h2>
    <p>You have successfully created your account, please verify to log in.</p>
    <a href="https://battleslaps.com/verify/${token}">Verify account</a>
</div>`
}
