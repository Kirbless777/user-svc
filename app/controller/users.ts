import { APIGatewayProxyEvent, Context } from 'aws-lambda';
import { Model } from 'mongoose';
import { MessageUtil } from '../utils/message';
import { UsersService } from '../service/users.service';
import { CreateUserDTO, SignInCredentials, UserResponse } from '../model/dto/createUserDTO';
import { MongoUser } from '../model/interfaces/user.interface';

export class UsersController extends UsersService {
  constructor (user: Model<MongoUser>) {
    super(user);
  }

  async create (event: APIGatewayProxyEvent, _?: Context) {
    const user: CreateUserDTO = JSON.parse(event.body);

    try {
      const result = await this.createUser(user);
      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.statusCode, err.msg, err);
    }
  }

  async signIn (event: APIGatewayProxyEvent, _?: Context) {
    const signIn: SignInCredentials = JSON.parse(event.body);
    try {
      const result = await this.userSignIn(signIn);
      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.statusCode, err.msg, err);
    }
  }

  async update (event: APIGatewayProxyEvent) {
    const id: number = Number(event.pathParameters.id);
    const body: object = JSON.parse(event.body);

    try {
      const result = await this.updateUser(id, body);
      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.code, err.message, err);
    }
  }

  async find () {
    try {
      const result = await this.findUsers();

      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.code, err.message, err);
    }
  }

  async findOne (event: APIGatewayProxyEvent, _: Context) {
    const id: string = event.pathParameters.id;

    try {
      const result = await this.findOneUserById(id);

      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.code, err.message, err);
    }
  }

  async deleteOne (event: APIGatewayProxyEvent) {
    const id: string = event.pathParameters.id;

    try {
      const result = await this.deleteOneUserById(id);

      if (result.deletedCount === 0) {
        return MessageUtil.error(1010, 'The data was not found! May have been deleted!');
      }

      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.code, err.message, err);
    }
  }

  async me (event: APIGatewayProxyEvent, _?: Context) {
    try {
      const token = event.headers['authorization'] || event.headers['Authorization'];
      const result: UserResponse = await this.findUserByToken(token);
      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.code, err.message, err);
    }
  }

  async verify (event: APIGatewayProxyEvent, _?: Context) {
    try {
      const token = event.headers['authorization'] || event.headers['Authorization'];
      const result: UserResponse = await this.verifyUser(token);
      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.statusCode, err.msg);
    }
  }

  async getUsers (event: APIGatewayProxyEvent, _?: Context) {
    const body: {
      emails: string[],
      includeIds?: boolean
    } = JSON.parse(event.body);

    try {
      const result = await this.findUsersByEmail(body.emails, body.includeIds ? 1 : 0);
      return MessageUtil.success(result);
    } catch (err) {
      return MessageUtil.error(err.code, err.message, err);
    }
  }

  async auth (event: APIGatewayProxyEvent, _?: Context) {
    try {
      const token = event.headers['authorization'] || event.headers['Authorization'];
      const authenticated: boolean = await this.authUser(token);
      if (authenticated) {
        return MessageUtil.success({ authenticated });
      }

      return MessageUtil.error(403, 'not authorized');

    } catch (err) {
      return MessageUtil.error(err.statusCode, err.msg, err);
    }
  }
}
