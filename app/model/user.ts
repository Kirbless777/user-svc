import mongoose from 'mongoose';
import { MongoUser } from './interfaces/user.interface';
import * as bcrypt from 'bcryptjs';
const SALT_WORK_FACTOR = 10;

const userSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  surname: { type: String, required: true },
  knownAs: { type: String },
  password: { type: String, required: true },
  registered: { type: Boolean, required: true, default: false },
  email: { type: String, required: true, index: { unique: true } },
  profileImage: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
});

userSchema.pre<MongoUser>('save', function (next) {
  // tslint:disable-next-line:no-this-assignment
  const user: MongoUser = this;

  // only hash the password if it has been modified (or is new)
  if (!this.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

// Note: OverwriteModelError: Cannot overwrite `Books` model once compiled. error
export const user = (mongoose.models.user ||
  mongoose.model<MongoUser>('user', userSchema, process.env.DB_USER_COLLECTION)
);

// import * as mongoose from 'mongoose'
// import { Schema } from 'mongoose'
// import { MongoUser } from '../interfaces/user.interface'
// import * as bcrypt from 'bcryptjs'
// const SALT_WORK_FACTOR = 10
//
// const UserSchema: Schema = new Schema({
//   firstName: { type: String, required: true },
//   surname: { type: String, required: true },
//   knownAs: { type: String },
//   password: { type: String, required: true },
//   registered: { type: Boolean, required: true, default: false },
//   email: { type: String, required: true, index: { unique: true } },
//   profileImage: { type: String, required: true },
// })
// UserSchema.pre<MongoUser>('save', function (next) {
//   // eslint-disable-next-line @typescript-eslint/no-this-alias
//   const user: MongoUser = this
//
//   // only hash the password if it has been modified (or is new)
//   if (!this.isModified('password')) return next()
//
//   // generate a salt
//   bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
//     if (err) return next(err)
//
//     // hash the password using our new salt
//     bcrypt.hash(user.password, salt, function (err, hash) {
//       if (err) return next(err)
//       // override the cleartext password with the hashed one
//       user.password = hash
//       next()
//     })
//   })
// })
//
// UserSchema.methods.comparePassword = function (candidatePassword, cb) {
//   bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
//     if (err) return cb(err)
//     cb(null, isMatch)
//   })
// }
//
// export default mongoose.models.User || mongoose.model<MongoUser>('User', UserSchema)
