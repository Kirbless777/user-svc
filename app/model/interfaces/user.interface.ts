import { Document } from 'mongoose';

export interface MongoUser extends Document {
  firstName: string;
  surname: string;
  email: string;
  password: string;
  knownAs?: string;
  registered?: boolean;
  profileImage: string;
}

export interface UserResponse {
  id: string;
  firstName: string;
  surname: string;
  knownAs?: string;
  email: string;
  profileImage: string;
  token?: string;
}
