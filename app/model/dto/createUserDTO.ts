export class CreateUserDTO {
  firstName: string;
  surname: string;
  knownAs?: string;
  password: string;
  email: string;
  profileImage: string;
}

export interface UserResponse {
  id: string;
  firstName: string;
  surname: string;
  knownAs?: string;
  email: string;
  profileImage: string;
  token?: string;
}

export interface SignInCredentials {
  email: string;
  password: string;
}
