import { ResponseVO } from '../model/vo/responseVo';

export interface IHeaders {
  [header: string]: string | boolean;
}

enum StatusCode {
  success = 200,
}

export const corsHeaders: IHeaders = {
  'Access-Control-Allow-Origin': '*', // Required for CORS support to work
  'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
  'Access-Control-Allow-Headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept',
  'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
};

class Result {
  readonly statusCode: number;
  readonly code: number;
  readonly message: string;
  readonly data?: any;

  constructor(statusCode: number, code: number, message: string, data?: any) {
    this.statusCode = statusCode;
    this.code = code;
    this.message = message;
    this.data = data;
  }

  /**
   * Serverless: According to the API Gateway specs, the body content must be stringified
   */
  bodyToString () {
    return {
      statusCode: this.statusCode,
      headers: corsHeaders,
      body: JSON.stringify({
        code: this.code,
        message: this.message,
        data: this.data,
      }),
    };
  }
}

export class MessageUtil {
  static success(data: object): ResponseVO {
    const result = new Result(StatusCode.success, 0, 'success', data);
    return result.bodyToString();
  }

  static error(statusCode: number = 500, message: string, code: number = 1000, err?: {}) {
    const result = new Result(statusCode, code, message, err);
    return result.bodyToString();
  }
}
