interface ErrorMessage<T = Record<string, unknown>> {
  statusCode: number;
  msg: string;
  data?: T;
  errors?: { [key: string]: string };
  'create-user-errors'?: { [key: string]: string };
}

// create actual interface with keys to map
const errors: { [key: string | number]: ErrorMessage } = {
  'no-user-body': {
    statusCode: 422,
    msg: 'no user object in post request',
  },
  'email-password': {
    statusCode: 403,
    msg: 'incorrect email or password',
  },
  'user-not-found': {
    statusCode: 404,
    msg: 'user not found',
  },
  'invalid-token': {
    statusCode: 401,
    msg: 'invalid token',
  },
  'user-not-validated': {
    statusCode: 401,
    msg: 'user not validated',
  },
  11000: {
    statusCode: 403,
    msg: 'duplicate email',
  }
};

const generateErrors: { [key: string]: (data) => ErrorMessage } = {
  'no-user-validation': (fields: string[]) => {
    const errors = {};
    fields.forEach((field: string) => {
      errors[field] = `${field} is a required field`;
    });
    return {
      statusCode: 422,
      msg: 'user required fields missing',
      'create-user-errors': errors,
    };
  },
};

export const throwApiError = (key: string): ErrorMessage => errors[key];
export const throwGeneratedApiError = (key: string, data: string[]): ErrorMessage =>
  generateErrors[key](data);

export const internalError = (e: Error): ErrorMessage<Error> => ({
  statusCode: 500,
  msg: 'oops, something went wrong...',
  data: e,
});
